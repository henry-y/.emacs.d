(require 'twiki)

(add-to-list 'auto-mode-alist'("\\.twiki$" . twiki-mode))

(provide 'setup-twiki)
