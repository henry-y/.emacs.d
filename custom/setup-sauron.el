(require 'sauron)

(setq sauron-hide-mode-line t
      sauron-sticky-frame t)

(provide 'setup-sauron)
